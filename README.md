# TTT
__Distributed Systems__ class project. TTT is a tic-tac-toe multiplayer internet game using Java RMI.

### RUNNING CLIENT
_runclient.sh_ -- runs client application

_clienttool.sh `<host>` `<command>`_ -- tool to troubleshoot remote registry problems

_java.policy_ -- this file is needed for security manager

### INSTALLATION
_installserver.sh `<dir>`_ -- installs the server files at specified directory

### RUNNING SERVER
_startrmiregistry.sh `<port>`_ -- starts rmiregistry on specified port; it must be started before running the server

_runserver.sh_ -- runs server

_servertool.sh `<command>`_ -- tool to troubleshoot TTT_engine problems in local registry

### DEVELOPMENT
_java.policy_ -- needs to be modified for development environment see the NOTE within it for more info

_server-vm-args_ -- these two arguments are required to invoke TTT_Engine can be pasted into Eclipse at `Run->Run Configurations` under the `Arguments` tab, then `VM arguments`

### DISTRIBUTION
_dist/client.sh_ -- creates TTT_Client.tar.gz for distribution

_dist/server.sh_ -- creates TTT_Server.tar.gz for distribution
