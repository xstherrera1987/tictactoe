package ttt;

import java.rmi.*;

// this interface describes the interactions that an implementing TTT engine
// should implement, and the functions that TTT clients can call to play TTT.
public interface TTT extends Remote {
	enum GameState {
		WIN, TIE, UNDECIDED, LOST, NOT_TURN_ERROR, BAD_MOVE_ERROR;
	}
	enum PlayerType {
		// spectator never actually uses its mark
		FIRST('X'), SECOND('O'), SPECTATOR('%');
		private final char mark;
		PlayerType(char m) {
			this.mark = m;
		}
	}
	
	// attempt to register with the remote TicTacToe game, return value is 
	// player type that has been assigned
	public PlayerType registerPlayer(String user, PlayerType type) throws RemoteException;
	
	// queries remote game object to see if game is in a ready state
	// 		returns true if two players have registered
	public boolean gameReady() throws RemoteException;
	
	// returns the name of player A
	public String getPlayer_A_Name() throws RemoteException;
	
	// returns the name of player B
	public String getPlayer_B_Name() throws RemoteException;
	
	// returns the winner of the previous game
	public String getPreviousWinner() throws RemoteException;
	
	// predicate method for checking a player's turn
	// 		returns true if it is this player's turn
	public boolean isTurn(String user) throws RemoteException;
	
	// the primary way of playing this game
	// make a move on the current game
	//		returns an int code describing the outcome
	public GameState makeMove(int row, int column, String usr) throws RemoteException;
	
	// predicate method for checking if a game is still valid
	// 		returns true if the game is over
	// NOTE: this method only returns true for a few seconds after a game is over
	// clients should set a timeout, and check with the more persistent function
	// getPreviousWinner()
	public boolean isGameOver() throws RemoteException;
	
	// returns the current grid with a SPACE character where no moves have occurred
	public char[][] getGrid() throws RemoteException;
	
	// adds a comment to the game feed
	public void makeComment(String s) throws RemoteException;
	
	// returns the index of the last comment made
	public int currentComment() throws RemoteException;
	
	// returns the comments indexed by num
	public String getComment(int num) throws RemoteException;
	
	// function that allows remote core dump for debugging
	//		returns a string that demonstrates internal state
	public String coreDump() throws RemoteException;
}