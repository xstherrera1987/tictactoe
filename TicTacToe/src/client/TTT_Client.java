package client;

import java.io.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.util.*;

import ttt.TTT;
// this class implements a client for TTT (RMI-based TicTacToe game)
public class TTT_Client {
	private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	private static String username = null;
	private static int commentsIndex = 0;
	private static ArrayList<String> comments = new ArrayList<String>();
	private static TTT.PlayerType mode = null;
	private static char[][] grid = new char[3][3];
	private static TTT game = null;
	private static Registry registry = null;
	// entry point into TTT_Client attempts to connect to a TTT game
	// at the locations pointed at by the argument to the program
	// or at the localhost if no arguments are supplied
	public static void main(String[] args) throws Exception {
		String host;
		if (args.length != 1) 
			host = "localhost";
		else
			host = args[0];
			
		registry = LocateRegistry.getRegistry(host);
		game = (TTT) registry.lookup("TTT");
		
		printWelcome();
		promptForUserName();
		boolean done = false;
		while (!done) {
			promptForType();
			if ( mode == TTT.PlayerType.FIRST ) attemptRegisteringAsPlayer();
			waitForStart();
			
			if (TTT.PlayerType.FIRST == mode || TTT.PlayerType.SECOND == mode )
				playGame();
			else
				watchGame();
			
			clearFields();
			done = !promptToReplay();
		}
	}
	// clears the fields for a new game
	private static void clearFields() {
		commentsIndex = 0;
		comments = new ArrayList<String>();
		grid = new char[3][3];
		mode = null;
	}
	// prompts the user to replay or quit the game
	private static boolean promptToReplay() {
		String input=null;
		boolean done = false;
		boolean answer = false;
		while (!done) {
			System.out.println("Would you like to participate in another game?");
			System.out.print("Enter Yes or No: ");
			try {
				input = in.readLine().trim();
			} catch (IOException e) { }
			
			if (input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes")) {
				done = true;
				answer = true;
			} else if (input.equalsIgnoreCase("n") || input.equalsIgnoreCase("no")) {
				done = true;
				answer = false;
			} else {
				done = false;
			}
		}
		return answer;
	}
	// prints the welcome string
	private static void printWelcome() {
		System.out.println("Welcome to Tic-Tac-Toe: a Java RMI multiplayer game");
	}
	// try to register with the remote game as a player
	private static void attemptRegisteringAsPlayer() throws Exception {
		TTT.PlayerType type = game.registerPlayer(username, TTT.PlayerType.FIRST );
		if (TTT.PlayerType.FIRST == type ) {
			mode = TTT.PlayerType.FIRST;
			System.out.println("Successfully registered as 1st player, using 'X'");
		} else if (TTT.PlayerType.SECOND == type) {
			mode = TTT.PlayerType.SECOND;
			System.out.println("Successfully registered as 2nd player, using 'O'");
		} else {
			mode = TTT.PlayerType.SPECTATOR;
			System.out.println("Sorry there are already 2 players, you must spectate");
		}
	}
	// game loop for watching a game
	private static void watchGame() {
		// NOTE: i want to allow spectators to comment on the game
		// however, calls to Reader.readLine() blocks, preventing updating the screen
		// TODO: try incorporating something like this:
		// http://www.javaspecialists.eu/archive/Issue153.html
		
		boolean done = false;
		while (!done) {
			try {
				done = game.isGameOver();
				if (!done) {
					getComments();
					grid = game.getGrid();
					displayGame();
					Thread.sleep(400);
				} else {
					System.out.println(game.getPreviousWinner() + " wins the game");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	// blocking method that queries the remote object until it is ready
	private static void waitForStart() {
		boolean start = false;
		while ( !start ) {
			try {
				start = game.gameReady();
				if (!start) {
					clearScreen();
					System.out.println("Waiting for enough players to join");
					Thread.sleep(2000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Game starting");
		try {
			System.out.println(game.getPlayer_A_Name() + " is using X");
			System.out.println(game.getPlayer_B_Name() + " is using O");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static void playGame() {
		boolean done = false;
		
		TTT.GameState outcome = TTT.GameState.NOT_TURN_ERROR;
		int choice, row, col;
		
		while (!done) {
			try {
				pollForTurn();
				if ( game.isGameOver() ) {
					done = true;
					outcome = TTT.GameState.LOST;
				}

				if (!done) {
					outcome = TTT.GameState.BAD_MOVE_ERROR;
					while (outcome == TTT.GameState.BAD_MOVE_ERROR) {
						choice = promptForPair();
						row = choice/3;
						col = choice%3; 
						outcome = game.makeMove(row,col,username);
						if (outcome == TTT.GameState.BAD_MOVE_ERROR)
							System.out.println("Cannot make a move there");
					}
					
					if (outcome == TTT.GameState.WIN || outcome == TTT.GameState.LOST 
							|| outcome == TTT.GameState.TIE) 
						done = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		switch(outcome) {
		case WIN: System.out.println("You won"); break;
		case TIE: System.out.println("The game was a tie"); break;
		case LOST: System.out.println("You lost"); break;
		}
	}
	// blocks (while updating screen) until it is this player's turn
	private static void pollForTurn() {
		boolean done = false;
		while (!done) {
			try {
				grid = game.getGrid();
				getComments();
				displayGame();
				done = game.isTurn(username);
				if ( game.isGameOver() )
					done = true;
				if (!done) Thread.sleep(333);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	// prints the grid and comments onto the screen
	private static void displayGame() {
		clearScreen();
		for (String s: comments)
			System.out.println(s);
		printGrid();
		System.out.println();
	}
	// retrieves any new comments from the remote object
	private static boolean getComments() throws Exception {
		int current = game.currentComment();
		if (current >= 0 && commentsIndex <= current) {
			for (; commentsIndex <= current; commentsIndex++) {
				if (!game.isGameOver());
					comments.add(commentsIndex, game.getComment(commentsIndex));
			}
			return true;
		}
		return false;
	}
	// prints the 3x3 tic tac toe grid
	private static void printGrid() {
		System.out.println("===1==2==3==");
		for (int i=0; i<3; i++) {
			System.out.print((char)('A'+i)+":");
			for (int j=0; j<3; j++) 
				System.out.print(" "+grid[i][j]+" ");
			System.out.println(":");
		}
		System.out.print("============");
	}
	// prints many new lines to the screen in order to clear previous output
	private static void clearScreen() {
		for (int i=0; i<50; i++) System.out.println();
	}
	// prompts players for playing or watching a game
	private static void promptForType() {
		boolean done = false;
		String input=null;
		char gameMode = (char) 0;
		while (!done) {
			System.out.println("Enter \"p\" to play");
			System.out.println("Enter \"w\" to watch");
			System.out.print("Play or spectate: ");
			try {
				input = in.readLine().trim();
				gameMode = input.charAt(0);
				
				if ('p' == gameMode || 'P' == gameMode) {
					done = true;
					mode = TTT.PlayerType.FIRST;
				} else if( 'w' == gameMode || 'W' == gameMode ) {
					done = true;
					mode = TTT.PlayerType.SPECTATOR;
				} else {
					done = false;
				}
			} catch (Exception e) { 
				System.out.println("Bad input");
			}
		}
	}
	// prompts the user to enter their name
	private static void promptForUserName() {
		String input=null;
		boolean done = false;
		while (!done) {
			System.out.println("Enter a username (maximum 12 characters)");
			System.out.print("What is your name: ");

			try {
				input = in.readLine().trim();
			} catch (IOException e) { }
			
			if (input.length() > 0 && input.length() <= 12)
				done = true;
			else
				done = false;
		}
		username = input;
	}
	// add a comment to the game feed
	private static void makeComment() {
		String comment = promptForComment();
		try {
			game.makeComment(username+":"+comment);
		} catch (RemoteException e) { }
	}
	// prompt the user to submit a comment
	private static String promptForComment() {
		String input=null;
		boolean done = false;
		while (!done) {
			System.out.print(username+" : ");

			try {
				input = in.readLine().trim();
			} catch (IOException e) { }
			
			if (input.length() > 0 && input.length() <= 12)
				done = true;
			else
				done = false;
		}
		return input;
	}
	// prompts the user for a cell position
	private static int promptForPair() {
		boolean done, num, letter;
		String input=null;
		String[] toks=null;
		char posLetter=(char)0;
		int posNum = Integer.MIN_VALUE;
		
		done = false;
		while (!done) {
			System.out.print("Enter a position pair: ");
			posNum = 0;
			posLetter = (char) 0;
			num=false;
			letter=false;
			
			try {
				input = in.readLine().trim();
			} catch (IOException e0) {	}
			
			toks = input.split(" ");
			if (2==toks.length) {
				try {
					posNum = Integer.parseInt(toks[0]);
					if ( posNum >= 1 && posNum <= 3)
						num = true;
				} catch (NumberFormatException e1) {
					try {
						posNum = Integer.parseInt(toks[1]);
						if ( posNum >= 1 && posNum <= 3)
							num = true;
					} catch (NumberFormatException e2) {
						num = false;
					}
				}
			} else if ( 1==toks.length ) {
				try {
					posNum = Integer.parseInt(input.charAt(0)+"");
					if ( posNum >= 1 && posNum <= 3)
						num = true;
				} catch (NumberFormatException e3) {
					try {
						posNum = Integer.parseInt(input.charAt(1)+"");
						if ( posNum >= 1 && posNum <= 3)
							num = true;
					} catch (NumberFormatException e4) {
						num = false;
					}
				}
			}
			
			if (num) {
				char fTokd=input.charAt(0);
				switch (fTokd) {
				case 'A': posLetter='A'; letter=true; break;
				case 'B': posLetter='B'; letter=true; break;
				case 'C': posLetter='C'; letter=true; break;
				case 'a': posLetter='a'; letter=true; break;
				case 'b': posLetter='b'; letter=true; break;
				case 'c': posLetter='c'; letter=true; break;
				}
				if (!letter) {
					if (2==toks.length) {
						if (1==toks[0].length() && 1==toks[1].length()) {
							char sTokd=toks[1].charAt(0);
							switch (sTokd) {
							case 'A': posLetter='A';
							case 'B': posLetter='B';
							case 'C': posLetter='C';
							case 'a': posLetter='a';
							case 'b': posLetter='b';
							case 'c': posLetter='c'; letter=true; break;
							}
						}
					} else if (1==toks.length && toks[0].length() > 1) {
						char sCatd=input.charAt(1);
						switch (sCatd) {
						case 'A': posLetter='A'; letter=true; break;
						case 'B': posLetter='B'; letter=true; break;
						case 'C': posLetter='C'; letter=true; break;
						case 'a': posLetter='a'; letter=true; break;
						case 'b': posLetter='b'; letter=true; break;
						case 'c': posLetter='c'; letter=true; break;
						}
					}
				}
			}
			
			done = num && letter;
		}
		
		int retVal = Integer.MIN_VALUE;
		switch(posLetter) {
		case 'A': posLetter='A';
		case 'a': posLetter='a'; retVal = 0; break;
		case 'B': posLetter='B';
		case 'b': posLetter='b'; retVal = 3; break;
		case 'C': posLetter='C';
		case 'c': posLetter='c'; retVal = 6; break;
		}
		retVal += (posNum - 1);
		return retVal;
	}
}