package server;

import java.rmi.*;
import java.util.*;
import ttt.TTT;
// this class implements a game of TicTacToe (by implementing TTT remote interface)
// many TTT_Engine objects can be registered in a single RMI registry, and any of
// these objects can be reused for various games

public class TTT_Engine implements TTT {
	String pA,pB;
	boolean aTurn, gameOver;
	ArrayList<String> comments = new ArrayList<String>();
	char[][] grid = new char[3][3];
	int plays;
	String prevWin = null;
	
	// initializes this remote object
	public TTT_Engine() {
		clear();
	}
	// clears the fields for a new game
	public void clear() {
		plays = 0;
		for (int i=0; i<9; i++)
			grid[i/3][i%3] = ' ';
		pA = pB = null;
		aTurn = true;
		gameOver = false;
		comments = new ArrayList<String>();
	}
	// attempt to register with the remote TicTacToe game, return value is 
	// player type that has been assigned
	public PlayerType registerPlayer(String user, PlayerType type) throws RemoteException {
		if (type == PlayerType.FIRST ) {
			if (null==pA) {
				pA = user;
				return PlayerType.FIRST;
			} else if (null==pB) {
				pB = user;
				return PlayerType.SECOND;
			} else {
				makeComment(user+" is spectating");
				return PlayerType.SPECTATOR;
			}
		} else if (type == PlayerType.SECOND) { 
			if (null==pB) {
				pB = user;
				return PlayerType.SECOND;
			} else if (null==pA) {
				pA = user;
				return PlayerType.FIRST;
			} else {
				makeComment(user+" is spectating");
				return PlayerType.SPECTATOR;
			}
		} else {
			makeComment(user+" is spectating");
			return PlayerType.SPECTATOR;
		}
	}
	// queries remote game object to see if game is in a ready state
	// 		returns true if two players have registered
	public boolean gameReady() throws RemoteException {
		return pA != null && pB != null;
	}
	// returns the name of player A
	public String getPlayer_A_Name() throws RemoteException {
		if (pA != null) return pA;
		else return "";
	}
	// returns the name of player B
	public String getPlayer_B_Name() throws RemoteException {
		if (pB != null) return pB;
		else return "";
	}
	// predicate method for checking a player's turn
	// 		returns true if it is this player's turn
	public boolean isTurn(String user) throws RemoteException {
		return (user.equalsIgnoreCase(pA) && aTurn) || 
				(user.equalsIgnoreCase(pB) && !aTurn);
	}
	// returns the winner of the previous game
	public String getPreviousWinner() throws RemoteException {
		return prevWin == null ? "" : prevWin;
	}
	// the primary way of playing this game
	// make a move on the current game
	//		returns an int code describing the outcome
	public GameState makeMove(int row, int column, String usr) throws RemoteException {
		GameState outcome;
		if (!gameOver) {
			if ( aTurn && usr.equals(pA) ) {
		    	if (grid[row][column] == ' ') {
			    	grid[row][column] = 'X';
			    	plays++;
			    	aTurn = false;
			    	makeComment(pA+" makes X at "+ getCell(row,column));
			    	outcome = outcome(row, column);
		    	} else return GameState.BAD_MOVE_ERROR;
			} else if ( !aTurn && usr.equals(pB)) {
				if (grid[row][column] == ' ') {
			    	grid[row][column] = 'O';
			    	plays++;
			    	aTurn = true;
			    	makeComment(pB+" makes O at "+ getCell(row,column));
			    	outcome = outcome(row, column);
				} else return GameState.BAD_MOVE_ERROR;
			} else {
				return GameState.NOT_TURN_ERROR;
			}
			
			if (outcome == GameState.WIN) {
				gameOver = true;
				prevWin = usr;
				try {
					Thread.sleep(1250);
				} catch (Exception e) {
					e.printStackTrace();
				}
				clear();
			}
			return outcome;
		} else {
			try {
				Thread.sleep(1250);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return GameState.LOST;
		}
	}
	// predicate method for checking if a game is still valid
	// 		returns true if the game is over
	// NOTE: this method only returns true for a few seconds after a game is over
	// clients should set a timeout, and check with the more persistent function
	// getPreviousWinner()
	public boolean isGameOver() throws RemoteException {
		return gameOver;
	}
	// returns the current grid with a SPACE character where no moves have occurred
	public char[][] getGrid() throws RemoteException {
		return grid;
	}
	// evaluates the game state after making a move at the specified cell
	//		returns an int code describing the outcome
	private GameState outcome(int row, int column) throws RemoteException {
		if (9==plays) return GameState.TIE;
		
		char symbol = grid[row][column];
		if (1==row && 1==column) {
			if (grid[1][1] == symbol) {
				if (grid[0][0]==symbol && grid[2][2]==symbol)
					return GameState.WIN;
				if (grid[0][2]==symbol && grid[2][0]==symbol)
					return GameState.WIN;
			}
		}
		
		boolean done;
		int i, j;
		
		done = false;
		for (i=0; done==false && i<3; i++)
			if (grid[row][i] != symbol)
				done = true;
		if (done == false) return GameState.WIN;
		
		done = false;
		for ( j=0; done==false && j<3; j++)
			if (grid[j][column] != symbol)
				done = true;
		if (done == false) return GameState.WIN;
	
		return GameState.UNDECIDED;
	}
	// adds a comment to the game feed
	public void makeComment(String s) throws RemoteException {
		comments.add(s);
	}
	// returns the index of the last comment made
	public int currentComment() throws RemoteException {
		return comments.size() - 1;
	}
	// returns the comments indexed by num
	public String getComment(int num) throws RemoteException {
		if (num < comments.size() && num >= 0)
			return comments.get(num);
		else
			return null;
	}
	// returns a two char representation of a cell for displaying to the user
	private String getCell(int row, int col) {
		char r = (char) 0;
		switch (row) {
		case 0: r='A'; break;
		case 1: r='B'; break;
		case 2: r='C'; break;
		}
		return "" + r + (col+1);
	}
	// DEBUG: fills the with random X and 0
	public void fillGridRandom() {
		Random r = new Random();
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				grid[i][j] = r.nextDouble() > .5 ? 'X' : 'O';
	}
	// function that allows remote core dump for debugging
	//		returns a string that demonstrates internal state
	public String coreDump() throws RemoteException {
		String commentsString,gridString;
		StringBuilder sb = new StringBuilder();
		sb.append("===1==2==3==");
		sb.append("\n");
		for (int i=0; i<3; i++) {
			sb.append((char)('A'+i)+":");
			for (int j=0; j<3; j++)
				sb.append(" "+grid[i][j]+" ");
			sb.append(":");
			sb.append("\n");
		}
		sb.append("============");
		gridString = sb.toString();
		
		if (comments != null) {
			sb = new StringBuilder();
			for (String s: comments) {
				sb.append(s);
				sb.append('\n');
			}
			commentsString = sb.toString();
		} else {
			commentsString = "No Comments";
		}
		
		sb = new StringBuilder();
		sb.append("pA=");
		sb.append( pA==null ? "null" : pA );
		sb.append("\n");
		sb.append("pB=");
		sb.append( pB==null ? "null" : pB );
		sb.append("\n");
		sb.append("aTurn=");
		sb.append(aTurn);
		sb.append("\n");
		sb.append("plays=");
		sb.append(plays);
		sb.append("\n");
		sb.append(gridString);
		sb.append("\n");
		sb.append(commentsString);
		return sb.toString();
	}
}