package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import ttt.TTT;

// binds one TTT_Engine as "TTT" on a local RMI registry
public class TTT_Server {
    public static void main(String[] args) throws Exception {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        
        TTT engine = new TTT_Engine();
        TTT stub = (TTT) UnicastRemoteObject.exportObject(engine, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("TTT", stub);
        System.out.println("TTT_Engine bound");  
    }
}
