package util;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class TTT_Tool {
	public static String host = null;
	public static String command = null;
	public static Registry registry = null;
	public static String[] remoteObjs = null;

	public static void initTool() {
		try {
			// NOTE: getRegistry() only creates a local reference to the remote registry and 
			// will succeed even if no remote registry exists 
			registry = LocateRegistry.getRegistry(host);
		} catch (RemoteException e) {
			System.out.println("Could not create local reference by LocateRegistry.getRegistry("+host+")");
			System.exit(1);
		}
	}
	
	public static void connectToRegistry() {
		try {
			remoteObjs = Naming.list(host);
		} catch (RemoteException e) {
			System.out.println("Registry not found.");
			System.exit(1);
		} catch (MalformedURLException e) {
			System.out.println("Your URL is malformed");
			System.exit(1);
		}
	}
}
