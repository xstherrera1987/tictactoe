package util;
import ttt.TTT;

public class TTT_ClientTool extends TTT_Tool {
	public static void main(String[] args) {
		host = args[1];
		command = args[2];
		
		initTool();
		connectToRegistry();
		
		if (command.equalsIgnoreCase("list")) {
			
		} else {
			printUsage();
		}
		
		System.out.println("Registry found");
		System.out.println("Registry " + host + " contains: ");
		if (remoteObjs.length == 0) {
			System.out.println("Registry is empty");
		} else {
			for (int i=0; i < remoteObjs.length; i++)
				System.out.println(remoteObjs[i]);
			
			// dump core of TTT_Engine
			try {
				TTT game = (TTT) registry.lookup("TTT");
				String d = game.coreDump();
				System.out.println( d );
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public static void printUsage() {
		System.out.println("java util.TTT_ClientTool <host> <command>");
		System.out.println("COMMANDS:");
		System.out.println("list\t");

	}
}
