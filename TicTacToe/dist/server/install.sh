#!/bin/bash
echo NOTE: source this script
if [ -d "$1" ];
then
        DIR="${1}"
else
        DIR="$(pwd)"/tttserver
	mkdir ${DIR}
fi

export TTT_INSTALL="${DIR}"
export CLASSPATH="${DIR}"/bin

mv tttserver.tar.gz "${DIR}"
pushd "${DIR}"
tar -xzf tttserver.tar.gz
rm tttserver.tar.gz
chmod +x *.sh

# finish java.policy file
echo -n '   permission java.io.FilePermission "' >> java.policy.incomplete
echo -n "${DIR}/bin/-" >> java.policy.incomplete
echo '", "read";' >> java.policy.incomplete
echo '};' >> java.policy.incomplete
mv java.policy.incomplete java.policy

popd
rm install.sh
