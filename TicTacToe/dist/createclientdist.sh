#!/bin/bash
cd dist/client
cp -r ../../bin/client .
cp -r ../../bin/ttt .
cp -r ../../bin/util .
rm util/TTT_ServerTool.class
jar cfm TTT_Client.jar MANIFEST.MF client/* ttt/* util/*
mv TTT_Client.jar ../../
rm -rf client ttt util
