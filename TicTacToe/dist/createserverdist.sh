#!/bin/bash
cd dist/server
cp -r ../../bin .
tar -czf tttserver.tar.gz java.policy.incomplete bin/* runserver.sh servertool.sh startrmiregistry.sh

# NOTE: add version number to top-level tarball
# eg. TTT_Server-1.0.3.tar.gz
tar -czf ../../TTT_Server.tar.gz tttserver.tar.gz install.sh README

rm tttserver.tar.gz
rm -rf bin
