#!/bin/bash
# NOTE: add a test to determine if this script was sourced
echo "NOTE: this script needs to be sourced in order to work properly"

ps -e | grep rmiregistry | kill `awk '{print $1}'`
sleep 1
CLASSPATH=$(pwd)/../bin/
export CLASSPATH
rmiregistry & 
